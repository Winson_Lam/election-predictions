
# coding: utf-8

# In[64]:

# import dependencies
import pandas as pd
import matplotlib.pyplot as plt
import time
import ast
import datetime


data = pd.read_csv('/Users/Winson/DynamicWeb/DynamicWeb/App/Analysis/General_Election2017_Candidate_Analysis 2.csv', low_memory=False)
df = data.drop_duplicates(subset = ['Candidate', 'body', 'date','dateTime','eventUri','id','isDuplicate','lang','sim','time','title',
 'uri','url','userHasPermissions','wgt'])

df1 = df[['date', 'Candidate', 'body']]

def string_to_dic(x):
    try:
        y = ast.literal_eval(x)
        return y
    except:
        return 'error:' + str(x)
    
df['Journal'] = df['source'].apply(lambda x: string_to_dic(x))

df_journal = df['Journal'].apply(pd.Series)

df2 = df1.join(df_journal)
df2 = df2.dropna(subset = ['uri'])

#df2.groupby(['title', 'Candidate']).size().unstack().head()

JC = df2[df2["Candidate"] == 'Jeremy Corbyn'].groupby('title').size().sort_values(ascending = False)
TM = df2[df2["Candidate"] == 'Theresa May'].groupby('title').size().sort_values(ascending = False)
TF = df2[df2["Candidate"] == 'Tim Farron'].groupby('title').size().sort_values(ascending = False)
TM = pd.DataFrame(TM.head()).reset_index().rename(columns = {'title': 'Newspaper', 0: 'volume'})
TF = pd.DataFrame(TF.head()).reset_index().rename(columns = {'title': 'Newspaper', 0: 'volume'})
JC = pd.DataFrame(JC.head()).reset_index().rename(columns = {'title': 'Newspaper', 0: 'volume'})

NP_TF = TF.Newspaper.ix[:].values
NPV_TF = TF.volume.ix[:].values
NP_TM = TM.Newspaper.ix[:].values
NPV_TM = TM.volume.ix[:].values
NP_JC = JC.Newspaper.iloc[:].values
NPV_JC = JC.volume.ix[:].values
