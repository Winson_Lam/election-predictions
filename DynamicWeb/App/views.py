
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, render_to_response
from models import posts
import Newspapers as NP
import json




def home(request):
	posting = posts.objects.all()[:2] # send and recieve data call from mysql

	content = {
	'data_x' : [str(i) for i in NP.NP_TM],
	'data_y': list(NP.NPV_TM),

	}

	return render_to_response('index.html', content)
	


	

# The index page relies on the dictionary in the render_to_response method, within it, 
# a large dictionary can be built


